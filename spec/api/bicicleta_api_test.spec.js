var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');
var server = require('../../bin/www');
var request = require('request');

var base_url = "http://localhost:3000/api/bicicletas";

describe("Bicicleta API", function() {
    beforeEach(function(done){    
        var mongoDB = 'mongodb://localhost:27017/red_bicicletas'; 
        mongoose.connect(mongoDB,{useNewUrlParser:true});

        const db = mongoose.connection;
        db.on('error',console.error.bind(console, "MongoDB coneccion error:"));
        db.once('open',function(){
            console.log('Conectado a la base de datos: ');
            done();
        });
    });

    afterEach(function(done){
        Bicicleta.deleteMany({},function(err,success){
            if(err) console.log(err);
            done();
        })
    });

    describe("GET BICICLETAS /", function() {
        it("Status 200", function(done) {
            request.get(base_url, function(error, response, body){
                var result = JSON.parse(body); 
                expect(response.statusCode).toBe(200);
                expect(result.bicicletas.length).toBe(0);
                done();
            }); 
        });
    });

    describe("POST BICICLETAS /create", function() {
        it("STATUS 200", (done) => {
            var headers = {'content-type' : 'application/json'};
            var aBici = '{"code":10, "color":"rojo", "modelo":"urbana", "lat":-38, "lng":-72 }';
            
            request.post({
                headers:    headers,
                url:        base_url+'/create',
                body:       aBici
            },  function(error, response, body){
                    expect(response.statusCode).toBe(200);
                    var bici = JSON.parse(body).bicicleta;
                  //  console.log(bici);
                  //  expect(bici.color).toBe("rojo");
                  //  expect(bici.ubicacion[0]).toBe(-38);
                  //  expect(bici.ubicacion[1]).toBe(-72);
                    done();
            });           
        });
    });

    describe("DELETE BICICLETAS /delete",function() {
        it("STATUS 204", (done) => {
            var a = Bicicleta.createInstance(1,'rojo','urbana',[-38.738, -72.575]);
            Bicicleta.add(a,function(err,newbici){
                var headers = {'content-type' : 'application/json'};
                  
                /*request.post({
                    headers:    headers,
                    url:        base_url+'/delete',
                    body:       1
                },  function(error, response, body){
                        console.log(response);
                        //expect(response.statusCode).toBe(204);
                        done();
                }); */
                done();
            });  
        });
    });


});