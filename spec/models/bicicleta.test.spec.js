var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');
const bicicleta = require('../../models/bicicleta');

describe("Test Bicicletas", function() {
    beforeEach(function(done){    
        var mongoDB = 'mongodb://localhost:27017/red_bicicletas'; 
        mongoose.connect(mongoDB,{useNewUrlParser:true});

        const db = mongoose.connection;
        db.on('error',console.error.bind(console, "MongoDB coneccion error:"));
        db.once('open',function(){
            console.log('Conectado a la base de datos: ');
            done();
        });
    });

    afterEach(function(done){
        Bicicleta.deleteMany({},function(err,success){
            if(err) console.log(err);
            done();
        })
    });

    describe("Bicicleta.createInstance", function() {
        it("creamos una instancia de bicicleta", function() {
            var bici = Bicicleta.createInstance(1,'rojo','urbana',[-38.738, -72.575]);

            expect(bici.code).toBe(1);
            expect(bici.color).toBe('rojo');
            expect(bici.modelo).toBe('urbana');
            expect(bici.ubicacion[0]).toEqual(-38.738);
            expect(bici.ubicacion[1]).toEqual(-72.575);            
        });
    });

    describe("Bicicleta.AllBicis", function() {
        it("comienza vacio", function(done) {
                Bicicleta.allBicis(function(err,bicis){
                expect(bicis.length).toBe(0);
                done();
             })         
        });
    });

    describe("Bicicleta.add",function() {
        it("Agrega solo una bici", function(done) {
                var abici = new Bicicleta({code: 1,color:'verde',modelo:'urbana'}); //[-38.738, -72.575]
                Bicicleta.add(abici,function(err,newbici){
                    if(err) console.log(err);
                    Bicicleta.allBicis(function(err,bicis){
                        expect(bicis.length).toBe(1);
                        expect(bicis[0].code).toBe(abici.code);
                        done();
                    })   
                })                         
        });
    });

    describe("Bicicleta.findByCode",function() {
        it("Debe devolver la bicicleta con code 1", function(done) {
            Bicicleta.allBicis(function(err,bicis){
                expect(bicis.length).toBe(0);
            
                
                var abici = new Bicicleta({code:1,color:'verde',modelo:'urbana'}); //[-38.738, -72.575]
                Bicicleta.add(abici,function(err,newbici){
                    if(err) console.log(err);
                    var abici2 = new Bicicleta({code:2,color:'rojo',modelo:'urbana'});
                    Bicicleta.add(abici2,function(err,newbici){
                        if(err) console.log(err);
                        Bicicleta.findByCode(1, function(error,targetBici){
                            expect(targetBici.code).toBe(abici.code);
                            expect(targetBici.color).toBe(abici.color);
                            expect(targetBici.modelo).toBe(abici.modelo);
                            
                            done();
                        });   
                    }); 
                });
            });    
                
        });
    });

    
    

});

/*
beforeEach(() => { Bicicleta.allBicis=[]; });

describe("Bicicleta.allBicis", function() {
    it("Comienza vacio", function() {
        expect(Bicicleta.allBicis.length).toBe(0);
    });
});

describe("Bicicleta.add", function() {
    it("Agregamos Una", function() {
        expect(Bicicleta.allBicis.length).toBe(0);
        var a = new Bicicleta(1,'rojo','urbana',[-38.738, -72.575]);
        Bicicleta.add(a);
        expect(Bicicleta.allBicis.length).toBe(1);
        expect(Bicicleta.allBicis[0]).toBe(a);
        
    });
}); 

describe("Bicicleta.remove", function() {
    
    it("Removemos Una", function() {
        expect(Bicicleta.allBicis.length).toBe(0);
        var a = new Bicicleta(1,'rojo','urbana',[-38.738, -72.575]);
        Bicicleta.add(a);

        expect(Bicicleta.allBicis.length).toBe(1);
        Bicicleta.removeById(1);
        expect(Bicicleta.allBicis.length).toBe(0);
        
    });
});


describe("Bicicleta.findById", function() {
    it("debe devolver la bici con ID 1", function() {
        expect(Bicicleta.allBicis.length).toBe(0);
        var bici_1 = new Bicicleta(1,'verde','urbana',[-38.738, -72.575]);
        var bici_2 = new Bicicleta(2,'rojo','urbana',[-38.728, -72.475]);
        Bicicleta.add(bici_1);
        Bicicleta.add(bici_2);

        var targetBici = Bicicleta.findById(1);
        expect(targetBici.id).toBe(1);
        expect(targetBici.color).toBe(bici_1.color);
        expect(targetBici.modelo).toBe(bici_1.modelo);

    });
});
*/
