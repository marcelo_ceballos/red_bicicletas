var mymap = L.map('main_map').setView([-38.7287589,-72.57510700], 13);

	L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
		maxZoom: 18,
		attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
			'<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
			'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
		id: 'mapbox/streets-v11',
		tileSize: 512,
		zoomOffset: -1
	}).addTo(mymap);

	L.marker([-38.728, -72.575]).addTo(mymap)
		.bindPopup("<b>Punto de encuentro!</b><br /> Temuco - Chile").openPopup();

	L.circle([-38.727232, -72.574434], 500, {
		color: 'red',
		fillColor: '#f03',
		fillOpacity: 0.5
	}).addTo(mymap).bindPopup("Este es el radio");

/*	L.polygon([
		[-38.716, -72.553],
		[-38.715, -72.575],
		[-38.714, -72.560]
	]).addTo(mymap).bindPopup("Este es un poligono.");
*/	

	$.ajax({
		dataType:"json",
		url:"api/bicicletas",
		success:function(result){
			console.log(result);
			result.bicicletas.forEach(function(bici){

				L.marker(bici.ubicacion).addTo(mymap)
		.bindPopup("<b>Bicicleta"+bici.id+"</b><br />"+bici.modelo+"-"+bici.color).openPopup();

			});
		}
	})

	var popup = L.popup();

	function onMapClick(e) {
		popup
			.setLatLng(e.latlng)
			.setContent("You clicked the map at " + e.latlng.toString())
			.openOn(mymap);
	}

	mymap.on('click', onMapClick);